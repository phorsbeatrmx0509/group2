$(function () {
  $("#showAll").click(function () {
    $(".target").show();
  });

  $(".app").click(function () {
    $(".target").hide();
    $("#app").show();
  });
  $(".card-filter").click(function () {
    $(".target").hide();
    $("#card").show();
  });
  $(".web-filter").click(function () {
    $(".target").hide();
    $("#web").show();
  });
});

const activePage = window.location.pathname;
const navLinks = document.querySelectorAll("nav a").forEach((link) => {
  if (link.href.includes(`${activePage}`)) {
    link.classList.add("active");
  }
});

const btnLink0 = $(".menu button[target='0']").click(function () {
  $(".menu button[target='0']")
    .css("background-color", "#dc3545")
    .css("color", "#fff");
  $(".menu button[target='1']").removeAttr("style");
  $(".menu button[target='2']").removeAttr("style");
  $(".menu button[target='3']").removeAttr("style");
});
const btnLink1 = $(".menu button[target='1']").click(function () {
  $(".menu button[target='1']")
    .css("background-color", "#dc3545")
    .css("color", "#fff");

  $(".menu button[target='0']").removeAttr("style");
  $(".menu button[target='2']").removeAttr("style");
  $(".menu button[target='3']").removeAttr("style");
});
const btnLink2 = $(".menu button[target='2']").click(function () {
  $(".menu button[target='2']")
    .css("background-color", "#dc3545")
    .css("color", "#fff");
  $(".menu button[target='0']").removeAttr("style");

  $(".menu button[target='1']").removeAttr("style");
  $(".menu button[target='3']").removeAttr("style");
});
const btnLink3 = $(".menu button[target='3']").click(function () {
  $(".menu button[target='3']")
    .css("background-color", "#dc3545")
    .css("color", "#fff");
  $(".menu button[target='0']").removeAttr("style");
  $(".menu button[target='1']").removeAttr("style");
  $(".menu button[target='2']").removeAttr("style");
});

const parentContainer = document.querySelectorAll(".h-100");

parentContainer.forEach((parentContainer) => {
  parentContainer.addEventListener("click", (e) => {
    const current = e.target;

    const isReadMoreBtn = current.classList.contains("btn-dark");

    if (!isReadMoreBtn) return;

    const currentText = e.target
      .closest(".card-body")
      .querySelector(".read-more-text");

    currentText.classList.toggle("read-more-text--show");

    current.textContent = current.textContent.includes("Read More")
      ? "Read Less..."
      : "Read More";
  });
});
